class BooksController < ApplicationController
  before_filter :fetch_book, :except => [:index, :create]

  def fetch_book
    @book = Book.find_by_id(params[:id])
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      render json: @book
    else
      render json: {status: :unprocessable_entity}
    end
  end

  def destroy
    if @book.destroy
      render json: {status: :ok}
    else
      render json: {status: :unprocessable_entity}
    end
  end

  def index
    @books = Book.all
    render json: @books
  end

  def show
    render json: @book
  end

  def update
    if @book.update_attributes(book_params)
      render json: {status: :ok}
    else
      render json: {status: :unprocessable_entity}
    end
  end

  def book_params
    params.require(:book).permit(:title)
  end
end
